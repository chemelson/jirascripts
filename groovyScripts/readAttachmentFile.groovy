import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.util.io.InputStreamConsumer

def issue = ComponentAccessor.issueManager.getIssueObject("JIRA-2285")

def am = ComponentAccessor.attachmentManager
def apm = ComponentAccessor.attachmentPathManager

def attachments = am.getAttachments(issue)

def attachment = attachments.first()

InputStreamConsumer consumer = { inputStream -> 
	File tempFile = File.createTempFile('attach', null);
    tempFile.withOutputStream { outputStream ->
    	outputStream << inputStream
    }
    return tempFile
} as InputStreamConsumer

File file = am.streamAttachmentContent(attachment, consumer)
def content = file.text
file.deleteOnExit()
content