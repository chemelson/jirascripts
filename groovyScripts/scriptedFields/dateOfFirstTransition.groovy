/*
Usage: create new scripted field and insert this code.
Use Template - Date Time Picker and Searcher - Date Time Range picker.
*/
import com.atlassian.jira.component.ComponentAccessor

def changeHistoryManager = ComponentAccessor.getChangeHistoryManager()
def created = changeHistoryManager.getChangeItemsForField(issue, "status").find {
    it.toString == "In progress"
}?.getCreated()

def createdTime = created?.getTime()

createdTime ? new Date(createdTime) : null
