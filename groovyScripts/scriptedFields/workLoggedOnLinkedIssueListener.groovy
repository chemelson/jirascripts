package com.example

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.index.IssueIndexingService

def issue = event.issue
def issueLinkManager = ComponentAccessor.getIssueLinkManager()
def issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService.class)

issueLinkManager.getInwardLinks(issue.id).each { issueLink ->
    if (issueLink.issueLinkType.name == "Composition") {
        def linkedIssue = issueLink.getSourceObject()
        issueIndexingService.reIndexIssueObjects([linkedIssue])
    }
}
