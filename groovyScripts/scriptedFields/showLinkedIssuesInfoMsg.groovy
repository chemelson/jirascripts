package com.example

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import groovy.xml.MarkupBuilder
import com.atlassian.jira.config.properties.APKeys

def issueLinkManager = ComponentAccessor.getIssueLinkManager()
def baseUrl = ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL)

def List<Issue> blockingIssues = []

issueLinkManager.getInwardLinks(issue.id).each { issueLink ->
    if (issueLink.issueLinkType.name == "Blocks") {
        def linkedIssue = issueLink.sourceObject
        if (!linkedIssue.assigneeId && !linkedIssue.getResolution()) {
            blockingIssues.add(linkedIssue)
        }
    }
}

if (blockingIssues) {
    StringWriter writer = new StringWriter()
    MarkupBuilder builder = new MarkupBuilder(writer)

    builder.div(class:"aui-message error shadowed") {
        p (class:"title") {
            span(class:"aui-icon icon-error", "")
            strong ("This issue is blocked by the following unresolved, unassigned issue(s):")
        }

        ul {
            blockingIssues.each {anIssue ->
                li {
                    a(href:"$baseUrl/browse/${anIssue.key}", anIssue.key)
                    i(": ${anIssue.summary} (${anIssue.status.name})")
                }
            }
        }
    }

    return writer
}
else {
    return null
}