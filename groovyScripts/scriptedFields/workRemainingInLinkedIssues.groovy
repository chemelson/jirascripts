import com.atlassian.jira.component.ComponentAccessor

def issueLinkManager = ComponentAccessor.getIssueLinkManager()

def totalRemaining = 0
issueLinkManager.getOutwardLinks(issue.id).each { issueLink ->
    if (issueLink.issueLinkType.name == "Composition") {
        def linkedIssue = issueLink.destinationObject
        totalRemaining += linkedIssue.getEstimate() ?: 0
    }
}

if (totalRemaining) {
    totalRemaining += issue.getEstimate() ?: 0
}

return totalRemaining as Long ?: 0L